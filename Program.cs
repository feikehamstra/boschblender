﻿using BoschBlender.Model;
using System;

namespace BoschBlender
{
    class Program
    {
        private static Blender _blender;
        static void Main(string[] args)
        {
            _blender = new Blender();

            Console.WriteLine(_blender.ToString());

            Run(Console.ReadLine());
        }

        private static void Run(string command)
        {
            switch (command)
            {
                case "on":
                    _blender.On();
                    break;
                case "off":
                    _blender.Off();
                    break;
                case "veg":
                    _blender.BlendVegetables();
                    break;
                case "ice":
                    _blender.BlendIceCubs();
                    break;
                case "bev":
                    _blender.BlendBeverage();
                    break;
                case "pulse":
                    _blender.Pulse();
                    break;
                default:
                    _blender.On();
                    break;
            }

            Console.WriteLine(_blender.ToString());

            Run(Console.ReadLine());
        }
    }
}
