using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoschBlender.Model;
using BoschBlender.Model.States;
using System.Collections.Generic;
using System;

namespace BoschBlenderTest
{
    [TestClass]
    public class BlenderTest
    {
        [TestMethod]
        public void AssertActionsOnStates()
        {
            BlenderEngine engine = new BlenderEngine();
            BlenderPane pane = new BlenderPane();

            for (int i = 0; i < 6; i++)
            {
                (Type Actual, Type Expected) assertion = AssertState<BlenderOffState>(i, engine, pane);
                Assert.AreEqual(assertion.Actual, assertion.Expected, ("BlenderOffState action " + i + " expected " + assertion.Expected + " but returned " + assertion.Actual));

                assertion = AssertState<BlenderOnState>(i, engine, pane);
                Assert.AreEqual(assertion.Actual, assertion.Expected, ("BlenderOnState action " + i + " expected " + assertion.Expected + " but returned " + assertion.Actual));

                assertion = AssertState<BlenderVegetablesState>(i, engine, pane);
                Assert.AreEqual(assertion.Actual, assertion.Expected, ("BlenderVegetablesState action " + i + " expected " + assertion.Expected + " but returned " + assertion.Actual));

                assertion = AssertState<BlenderBeverageState>(i, engine, pane);
                Assert.AreEqual(assertion.Actual, assertion.Expected, ("BlenderBeverageState action " + i + " expected " + assertion.Expected + " but returned " + assertion.Actual));

                assertion = AssertState<BlenderIceCubesState>(i, engine, pane);
                Assert.AreEqual(assertion.Actual, assertion.Expected, ("BlenderIceCubesState action " + i + " expected " + assertion.Expected + " but returned " + assertion.Actual));

                assertion = AssertState<BlenderPulseState>(i, engine, pane);
                Assert.AreEqual(assertion.Actual, assertion.Expected, ("BlenderPulseState action " + i + " expected " + assertion.Expected + " but returned " + assertion.Actual));
            }
        }

        public (Type Actual, Type Expected) AssertState<T>(int action, params object[] args) where T : IBlenderState
        {
            IBlenderState state = (T)Activator.CreateInstance(typeof(T), args);

            if (state.GetType() == typeof(BlenderOffState))
            {
                switch (action)
                {
                    case 0:
                        return (state.Off().GetType(), typeof(BlenderOffState));
                    case 1:
                        return (state.On().GetType(), typeof(BlenderOnState));
                    case 2:
                        return (state.BlendVegetables().GetType(), typeof(BlenderOffState));
                    case 3:
                        return (state.BlendBeverage().GetType(), typeof(BlenderOffState));
                    case 4:
                        return (state.BlendIceCubes().GetType(), typeof(BlenderOffState));
                    case 5:
                        return (state.Pulse().GetType(), typeof(BlenderOffState));
                } 
            }

            if (state.GetType() == typeof(BlenderOnState))
            {
                switch (action)
                {
                    case 0:
                        return (state.Off().GetType(), typeof(BlenderOffState));
                    case 1:
                        return (state.On().GetType(), typeof(BlenderOnState));
                    case 2:
                        return (state.BlendVegetables().GetType(), typeof(BlenderVegetablesState));
                    case 3:
                        return (state.BlendBeverage().GetType(), typeof(BlenderBeverageState));
                    case 4:
                        return (state.BlendIceCubes().GetType(), typeof(BlenderIceCubesState));
                    case 5:
                        return (state.Pulse().GetType(), typeof(BlenderOnState));
                }
            }

            if (state.GetType() == typeof(BlenderVegetablesState))
            {
                switch (action)
                {
                    case 0:
                        return (state.Off().GetType(), typeof(BlenderOffState));
                    case 1:
                        return (state.On().GetType(), typeof(BlenderOnState));
                    case 2:
                        return (state.BlendVegetables().GetType(), typeof(BlenderOnState));
                    case 3:
                        return (state.BlendBeverage().GetType(), typeof(BlenderOnState));
                    case 4:
                        return (state.BlendIceCubes().GetType(), typeof(BlenderOnState));
                    case 5:
                        return (state.Pulse().GetType(), typeof(BlenderVegetablesState));
                }
            }

            if (state.GetType() == typeof(BlenderBeverageState))
            {
                switch (action)
                {
                    case 0:
                        return (state.Off().GetType(), typeof(BlenderOffState));
                    case 1:
                        return (state.On().GetType(), typeof(BlenderOnState));
                    case 2:
                        return (state.BlendVegetables().GetType(), typeof(BlenderOnState));
                    case 3:
                        return (state.BlendBeverage().GetType(), typeof(BlenderOnState));
                    case 4:
                        return (state.BlendIceCubes().GetType(), typeof(BlenderOnState));
                    case 5:
                        return (state.Pulse().GetType(), typeof(BlenderBeverageState));
                }
            }

            if (state.GetType() == typeof(BlenderIceCubesState))
            {
                switch (action)
                {
                    case 0:
                        return (state.Off().GetType(), typeof(BlenderOffState));
                    case 1:
                        return (state.On().GetType(), typeof(BlenderOnState));
                    case 2:
                        return (state.BlendVegetables().GetType(), typeof(BlenderOnState));
                    case 3:
                        return (state.BlendBeverage().GetType(), typeof(BlenderOnState));
                    case 4:
                        return (state.BlendIceCubes().GetType(), typeof(BlenderOnState));
                    case 5:
                        return (state.Pulse().GetType(), typeof(BlenderIceCubesState));
                }
            }

            if (state.GetType() == typeof(BlenderPulseState))
            {
                switch (action)
                {
                    case 0:
                        return (state.Off().GetType(), typeof(BlenderOffState));
                    case 1:
                        return (state.On().GetType(), typeof(BlenderPulseState));
                    case 2:
                        return (state.BlendVegetables().GetType(), typeof(BlenderPulseState));
                    case 3:
                        return (state.BlendBeverage().GetType(), typeof(BlenderPulseState));
                    case 4:
                        return (state.BlendIceCubes().GetType(), typeof(BlenderPulseState));
                    case 5:
                        return (state.Pulse().GetType(), typeof(BlenderPulseState));
                }
            } 
            return (null, null);
        }
    }
}
