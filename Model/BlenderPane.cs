﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model
{
    public class BlenderPane
    {
        private BlenderPaneColor _paneColor { get; set; } = BlenderPaneColor.Blue;
        private bool _on { get; set; } = false;

        public void On(BlenderPaneColor color)
        {
            _on = true;
            _paneColor = color;
        }

        public void Off()
        {
            _on = false;
        }

        public override string ToString()
        {
            return "- LED is " + (!_on ? "off" : ("on (color: " + _paneColor.ToString() + ")"));
        }
    }
}
