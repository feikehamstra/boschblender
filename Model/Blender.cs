﻿using BoschBlender.Model.States;
using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model
{
    public class Blender
    {
        private IBlenderState _state;

        public Blender() : this(new BlenderOnState(new BlenderEngine(), new BlenderPane())) { }

        public Blender(BlenderEngine engine, BlenderPane pane) : this(new BlenderOnState(engine, pane)) { }

        public Blender(IBlenderState initialState)
        {
            _state = initialState;
        }

        public void On()
        {
            _state = _state.On();
        }

        public void Off()
        {
            _state = _state.Off();
        }

        public void BlendVegetables()
        {
            _state = _state.BlendVegetables();
        }

        public void BlendIceCubs()
        {
            _state = _state.BlendIceCubes();
        }

        public void BlendBeverage()
        {
            _state = _state.BlendBeverage();
        }

        public void Pulse()
        {
            _state = _state.Pulse();
        }

        public override string ToString()
        {
            return _state.ToString();
        }
    }
}
