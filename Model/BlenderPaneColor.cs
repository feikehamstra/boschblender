﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model
{
    public enum BlenderPaneColor
    {
        Blue,
        Green,
        Red,
        Purple,
        White
    }
}
