﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public class BlenderOnState : BlenderState
    {
        public BlenderOnState(BlenderEngine engine, BlenderPane pane) : base(engine, pane)
        {
            _engine.On(0);
            _pane.On(BlenderPaneColor.Blue);
        }

        public override IBlenderState On()
        {
            _engine.On();
            return this;
        }

        public override IBlenderState Off()
        {
            _engine.Off();
            return new BlenderOffState(_engine, _pane);
        }

        public override IBlenderState BlendBeverage()
        {
            return _engine.GetCurrentSpeed() == 0 ? (IBlenderState) new BlenderBeverageState(_engine, _pane) : this;
        }

        public override IBlenderState BlendIceCubes()
        {
            return _engine.GetCurrentSpeed() == 0 ? (IBlenderState) new BlenderIceCubesState(_engine, _pane) : this;
        }

        public override IBlenderState BlendVegetables()
        {
            return _engine.GetCurrentSpeed() == 0 ? (IBlenderState) new BlenderVegetablesState(_engine, _pane) : this;
        }

        public override IBlenderState Pulse()
        {
            return this;
        }

        public override string ToString()
        {
            return (_engine.GetCurrentSpeed() == 0 ? "Turned On " : "Blending at speed " + _engine.GetCurrentSpeed() + ".. ") + base.ToString();
        }
    }
}
