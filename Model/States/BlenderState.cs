﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public abstract class BlenderState: IBlenderState
    {
        protected BlenderEngine _engine;
        protected BlenderPane _pane;

        protected BlenderState(BlenderEngine engine, BlenderPane pane)
        {
            _engine = engine;
            _pane = pane;
        }

        public virtual IBlenderState On()
        {
            return new BlenderOnState(_engine, _pane);
        }

        public virtual IBlenderState Off()
        {
            return new BlenderOffState(_engine, _pane);
        }

        public virtual IBlenderState BlendBeverage()
        {
            return new BlenderOnState(_engine, _pane);
        }

        public virtual IBlenderState BlendIceCubes()
        {
            return new BlenderOnState(_engine, _pane);
        }

        public virtual IBlenderState BlendVegetables()
        {
            return new BlenderOnState(_engine, _pane);
        }

        public virtual IBlenderState Pulse()
        {
            return this;
        }

        public override string ToString()
        {
            return _pane.ToString();
        }
    }
}
