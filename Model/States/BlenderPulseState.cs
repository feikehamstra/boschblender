﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BoschBlender.Model.States
{
    public class BlenderPulseState : BlenderState
    {
        public BlenderPulseState(BlenderEngine engine, BlenderPane pane): base(engine, pane)
        {
            _engine.On(9);
            _pane.On(BlenderPaneColor.Purple);
        }

        public override IBlenderState On()
        {
            return this;
        }

        public override IBlenderState Off()
        {
            return new BlenderOffState(_engine, _pane);
        }

        public override IBlenderState BlendBeverage()
        {
            return this;
        }

        public override IBlenderState BlendIceCubes()
        {
            return this;
        }

        public override IBlenderState BlendVegetables()
        {
            return this;
        }

        public override IBlenderState Pulse()
        {
            return this;
        }

        public override string ToString()
        {
            return "Pulsing.. " + base.ToString();
        }
    }
}
