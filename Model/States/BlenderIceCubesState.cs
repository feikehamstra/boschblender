﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public class BlenderIceCubesState : BlenderState
    {
        public BlenderIceCubesState(BlenderEngine engine, BlenderPane pane) : base(engine, pane)
        {
            _engine.On(3);
            _pane.On(BlenderPaneColor.White);
        }

        public override string ToString()
        {
            return "Blending Ice Cubes. " + base.ToString();
        }
    }
}
