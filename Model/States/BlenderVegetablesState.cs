﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public class BlenderVegetablesState : BlenderState
    {
        public BlenderVegetablesState(BlenderEngine engine, BlenderPane pane) : base(engine, pane)
        {
            _engine.On(6);
            _pane.On(BlenderPaneColor.Green);
        }

        public override string ToString()
        {
            return "Blending Vegetables.. " + base.ToString();
        }
    }
}
