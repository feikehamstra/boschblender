﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public interface IBlenderState
    {
        public IBlenderState On();
        public IBlenderState Off();
        public IBlenderState BlendVegetables();
        public IBlenderState BlendIceCubes();
        public IBlenderState BlendBeverage();
        public IBlenderState Pulse();
    }
}