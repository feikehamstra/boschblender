﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public class BlenderBeverageState : BlenderState
    {
        public BlenderBeverageState(BlenderEngine engine, BlenderPane pane) : base(engine, pane)
        {
            _engine.On(4);
            _pane.On(BlenderPaneColor.Red);
        }

        public override string ToString()
        {
            return "Blending Beverage.. " + base.ToString();
        }
    }
}
