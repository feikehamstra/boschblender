﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model.States
{
    public class BlenderOffState : BlenderState
    {
        public BlenderOffState(BlenderEngine engine, BlenderPane pane) : base(engine, pane)
        {
            _engine.Off();
        }

        public override IBlenderState Pulse()
        {
            return new BlenderPulseState(_engine, _pane);
        }

        public override IBlenderState Off()
        {
            return this;
        }

        public override IBlenderState BlendBeverage()
        {
            return this;
        }

        public override IBlenderState BlendIceCubes()
        {
            return this;
        }

        public override IBlenderState BlendVegetables()
        {
            return this;
        }

        public override string ToString()
        {
            return "Turned Off..";
        }
    }
}
