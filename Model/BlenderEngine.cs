﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoschBlender.Model
{
    public class BlenderEngine
    {
        private int _currentSpeed { get; set; }

        public BlenderEngine()
        {
            _currentSpeed = -1;
        }

        public void On(int speed)
        {
            _currentSpeed = speed;
        }

        public void On()
        {
            if (_currentSpeed < 10)
            {
                _currentSpeed++;
            }
        }

        public void Off()
        {
            _currentSpeed = -1;
        }
  
        public int GetCurrentSpeed()
        {
            return _currentSpeed;
        }
    }
}
